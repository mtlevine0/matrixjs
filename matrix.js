window.onload = function(){
	var width = window.innerWidth;
	var height = window.innerHeight;
	var c = document.getElementById("output");
	var ctx = c.getContext("2d");
	c.height = height;
	c.width = width;
	var drops = [];
	var fontSize = 10;
	var columns = width/fontSize;
	for(var x = 0; x < columns; x++){
		drops[x] = 1;
	}

	function drawDrops(){
		ctx.fillStyle = "rgba(0, 0, 0, 0.05)";
		ctx.fillRect(0, 0, width, height);
		ctx.fillStyle = "#0F0";
		ctx.font = fontSize + "px arial";

		for(var i = 0; i < drops.length; i+=1){
			ctx.fillText(String.fromCharCode(3e4+Math.random()*33), i*fontSize, drops[i]*fontSize);
			if(drops[i]*fontSize > height && Math.random() > 0.975){
				drops[i] = 0;
			}
			drops[i]++;
		}
	}
	setInterval(drawDrops,33);
};